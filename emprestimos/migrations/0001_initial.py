# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-04-21 16:34
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import djangoplus.db.models.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Administrador',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='Nome')),
                ('mail', djangoplus.db.models.fields.EmailField(max_length=255, verbose_name='E-mail')),
            ],
            options={
                'verbose_name': 'Administrador',
                'verbose_name_plural': 'Administradores',
            },
        ),
        migrations.CreateModel(
            name='Amigo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='Nome')),
                ('data_nascimento', djangoplus.db.models.fields.DateField(verbose_name='Data de Nascimento')),
                ('ativo', djangoplus.db.models.fields.BooleanField(default=True, verbose_name='Ativo')),
            ],
            options={
                'verbose_name': 'Amigo',
                'verbose_name_plural': 'Amigos',
            },
        ),
        migrations.CreateModel(
            name='Categoria',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descricao', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='Descri\xe7\xe3o')),
            ],
            options={
                'verbose_name': 'Categoria',
                'verbose_name_plural': 'Categorias',
            },
        ),
        migrations.CreateModel(
            name='Emprestimo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data_emprestimo', djangoplus.db.models.fields.DateField(verbose_name='Data do Empr\xe9stimo')),
                ('data_prevista_devolucao', djangoplus.db.models.fields.DateField(verbose_name='Data Prevista Devolu\xe7\xe3o')),
                ('data_devolucao', djangoplus.db.models.fields.DateField(null=True, verbose_name='Data da Devolu\xe7\xe3o')),
                ('amigo', djangoplus.db.models.fields.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='emprestimos.Amigo', verbose_name='Amigo')),
            ],
            options={
                'verbose_name': 'Empr\xe9stimo',
                'verbose_name_plural': 'Empr\xe9stimos',
            },
        ),
        migrations.CreateModel(
            name='Endereco',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('rua', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='Rua')),
                ('numero', djangoplus.db.models.fields.IntegerField(verbose_name='N\xfamero')),
                ('complemento', djangoplus.db.models.fields.CharField(blank=True, max_length=255, null=True, verbose_name='Complemento')),
            ],
            options={
                'verbose_name': 'Endere\xe7o',
                'verbose_name_plural': 'Endere\xe7os',
            },
        ),
        migrations.CreateModel(
            name='Node',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tree_index', djangoplus.db.models.fields.CharField(blank=True, editable=False, max_length=255, null=True)),
                ('name', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='Name')),
                ('parent', djangoplus.db.models.fields.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='emprestimos.Node')),
            ],
            options={
                'verbose_name': 'Node',
                'verbose_name_plural': 'Nodes',
            },
        ),
        migrations.CreateModel(
            name='Objeto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='Nome')),
                ('foto', djangoplus.db.models.fields.ImageField(blank=True, default=b'/static/images/blank.png', null=True, upload_to=b'objetos')),
                ('categoria', djangoplus.db.models.fields.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='emprestimos.Categoria', verbose_name='Categoria')),
            ],
            options={
                'verbose_name': 'Objeto',
                'verbose_name_plural': 'Objetos',
            },
        ),
        migrations.CreateModel(
            name='Pessoa',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='Nome')),
                ('mail', djangoplus.db.models.fields.EmailField(max_length=255, verbose_name='E-mail')),
                ('data_nascimento', djangoplus.db.models.fields.DateField(null=True, verbose_name='Data de Nascimento')),
                ('endereco', djangoplus.db.models.fields.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='emprestimos.Endereco', verbose_name='Endere\xe7o')),
            ],
            options={
                'verbose_name': 'Pessoa',
                'verbose_name_plural': 'Pessoas',
            },
        ),
        migrations.CreateModel(
            name='Subcategoria',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descricao', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='Descri\xe7\xe3o')),
                ('categoria', djangoplus.db.models.fields.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='emprestimos.Categoria', verbose_name=b'Categoria')),
            ],
            options={
                'verbose_name': 'Subcategoria',
                'verbose_name_plural': 'Subcategorias',
            },
        ),
        migrations.CreateModel(
            name='Telefone',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tipo', djangoplus.db.models.fields.CharField(choices=[('Fixo', 'Fixo'), ('Celular', 'Celular')], max_length=255, verbose_name='Tipo')),
                ('numero', djangoplus.db.models.fields.PhoneField(max_length=255, verbose_name='N\xfamero')),
            ],
            options={
                'verbose_name': 'Telefone',
                'verbose_name_plural': 'Telefones',
            },
        ),
        migrations.AddField(
            model_name='pessoa',
            name='telefones',
            field=djangoplus.db.models.fields.OneToManyField(to='emprestimos.Telefone', verbose_name='Telefones'),
        ),
        migrations.AddField(
            model_name='objeto',
            name='pessoa',
            field=djangoplus.db.models.fields.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='emprestimos.Pessoa', verbose_name='Pessoa'),
        ),
        migrations.AddField(
            model_name='objeto',
            name='subcategoria',
            field=djangoplus.db.models.fields.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='emprestimos.Subcategoria', verbose_name='Subcategoria'),
        ),
        migrations.AddField(
            model_name='emprestimo',
            name='objetos',
            field=djangoplus.db.models.fields.ManyToManyField(to='emprestimos.Objeto', verbose_name='Objetos'),
        ),
        migrations.AddField(
            model_name='emprestimo',
            name='pessoa',
            field=djangoplus.db.models.fields.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='emprestimos.Pessoa', verbose_name='Pessoa'),
        ),
        migrations.AddField(
            model_name='amigo',
            name='pessoa',
            field=djangoplus.db.models.fields.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='emprestimos.Pessoa', verbose_name='Pessoa'),
        ),
    ]
