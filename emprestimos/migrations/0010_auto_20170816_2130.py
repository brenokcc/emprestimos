# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-08-16 21:30
from __future__ import unicode_literals

from django.db import migrations
import django.db.models.deletion
import djangoplus.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('emprestimos', '0009_pessoa_enderecos'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pessoa',
            name='endereco',
            field=djangoplus.db.models.fields.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='enderecos.Endereco', verbose_name='Endere\xe7o Principal'),
        ),
        migrations.AlterField(
            model_name='pessoa',
            name='enderecos',
            field=djangoplus.db.models.fields.OneToManyField(related_name='x', to='enderecos.Endereco', verbose_name='Endere\xe7os Alternativos'),
        ),
    ]
