# -*- coding: utf-8 -*-
from djangoplus.ui.components import forms


class AlterarPrevisaoDevolucao(forms.Form):

    data_prevista_devolucao = forms.DateField(label=u'Nova Data de Previsão')

    class Meta:
        title = u'Alterar Previsão de Devolução'
        submit_label = title

