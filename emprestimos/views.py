# -*- coding: utf-8 -*-
from djangoplus.decorators.views import view, action, dashboard
from djangoplus.ui.components.calendar import ModelCalendar
from djangoplus.ui.components.panel import Panel, ModelPanel
from djangoplus.ui.components.report import StatisticsTable, ModelReport, ModelTable, Chart
from emprestimos.models import Emprestimo, Amigo, Objeto


@view(u'Relatório de Empréstimos por Objeto', can_view='Administrador', icon='fa-list', menu=u'Relatórios::Listagem')
def listagem(request):
    """
    Exibe uma tabela com todos os empréstimos.
    """
    paginator = ModelReport(request, u'Relatório de Empréstimos por Objeto', Emprestimo.objects.all().order_by('-id'), list_filter=('objetos', 'pessoa'))
    paginator.count('pessoa', 'objetos__categoria', add_chart=True)
    return locals()


@view(u'Relatório de Totais', can_view='Administrador', icon='fa-paperclip', menu=u'Relatórios::Totais', sequence=8)
def relatorios(request):
    """
    Exibe gráficos sobre os empréstimos realizados.
    """
    panel = Panel(None, u'Relatórios de Totais', 'Os relatórios abaixo apresentam o total de empréstimos.')
    qs = Emprestimo.objects.all()
    qs0 = qs.count('objetos')
    qs1 = qs.count('pessoa', 'objetos__categoria')
    qs2 = qs.count('data_emprestimo')
    qs3 = qs.count('data_emprestimo', 'pessoa')
    qs4 = Objeto.objects.all().sum('valor', 'pessoa', 'categoria')

    return locals()


@action(Amigo, u'Imprimir', can_execute='Pessoa', category=u'Relatórios', style='ajax pdf')
def imprimir_amigo(request, pk):
    amigo = Amigo.objects.get(pk=pk)
    panel = ModelPanel(request, amigo)
    panel.as_pdf = True
    table = ModelTable(request, u'Empréstimos', amigo.emprestimo_set.all())
    table.as_pdf = True
    title = amigo.nome
    return locals()


@dashboard(can_view='Pessoa', position='center')
def calendario(request):
    widget = ModelCalendar(request, 'Empréstimos', True)
    widget.add(Emprestimo.objects.all(request.user), 'data_emprestimo', 'data_prevista_devolucao')
    return locals()
