# -*- coding: utf-8 -*-
import datetime

from django.core.exceptions import ValidationError
from djangoplus.db import models
from djangoplus.decorators import action, subset, attr
from enderecos.models import Endereco


class Administrador(models.Model):
    """
    Usuário responsável pelo cadastro das categorias de objetos.
    """
    nome = models.CharField(u'Nome', example=u'Admin')
    mail = models.EmailField(u'E-mail', example=u'admin@emprestimos.com')

    fieldsets = (
        (u'Dados Gerais', {'fields': ('nome', 'mail'), }),
    )

    class Meta:
        verbose_name = u'Administrador'
        verbose_name_plural = u'Administradores'
        list_menu = u'Usuários::Administradores', 'fa-user-plus'
        role_username = 'mail'
        doc = True
        sequence = 1

    def __unicode__(self):
        return self.nome


class Categoria(models.Model):
    descricao = models.CharField(u'Descrição', search=True, example=u'Livros')
    categoria_pai = models.ForeignKey('emprestimos.Categoria', verbose_name=u'Categoria', null=True, blank=True, tree=True)


    fieldsets = (
        (u'Dados Gerais', {'fields': ('categoria_pai', 'descricao',), }),
    )

    class Meta:
        verbose_name = u'Categoria'
        verbose_name_plural = u'Categorias'
        can_admin = 'Administrador',
        list_menu = u'Cadastros::Gerais::Categorias', 'fa-th'
        add_shortcut = True
        list_shortcut = True
        icon = 'fa-th'
        verbose_female = True
        log = True
        doc = True
        sequence = 2
        pdf = True

    def __unicode__(self):
        return self.descricao


class Telefone(models.Model):

    tipo = models.CharField(u'Tipo', choices=((u'Fixo', u'Fixo'), (u'Celular', u'Celular')), example=u'Fixo')
    numero = models.PhoneField(u'Número', example=u'(84) 3272-3898')

    fieldsets = (
        (u'Dados Gerais', {'fields': ('tipo', 'numero')}),
    )

    class Meta:
        verbose_name = u'Telefone'
        verbose_name_plural = u'Telefones'

    def __unicode__(self):
        return u'(%s) %s' % (self.tipo, self.numero)


class Pessoa(models.Model):
    """
    Representa o usuário comum, o qual pode se cadastrar no sistema e registrar os empréstimos
    de seus objetos a seus amigos.
    """
    nome = models.CharField(u'Nome', search=True, example=u'Carlos Breno')
    mail = models.EmailField(u'E-mail', example=u'brenokcc@yahoo.com.br')
    data_nascimento = models.DateField(verbose_name=u'Data de Nascimento', null=True, example='27/08/1984')

    endereco = models.OneToOneField(Endereco, verbose_name=u'Endereço Principal', exclude=False, null=True, blank=True, display='detail')
    enderecos = models.OneToManyField(Endereco, verbose_name=u'Endereços Alternativos', exclude=False, blank=False, display='detail', related_name='x')

    telefones = models.OneToManyField(Telefone, verbose_name=u'Telefones', exclude=False, blank=False, display='detail')


    fieldsets = (
        (u'Dados Gerais', {'fields': (('nome', 'data_nascimento'), 'mail',),}),
        (u'Contato::', {'fields': ('endereco', 'enderecos')}),
        (u'Contato::', {'fields': ('telefones',)}),
        (u'Amigos::Amigos', {'relations': ('amigo_set',), }),
        (u'Objetos::Objetos', {'relations': ('objeto_set',), }),
    )

    class Meta:
        verbose_name = u'Pessoa'
        verbose_name_plural = u'Pessoas'
        can_admin = 'Administrador'
        list_menu = u'Usuários::Pessoas', 'fa-user-plus'
        add_shortcut = True
        list_shortcut = True
        icon = 'fa-user'
        verbose_female = True
        role_name = 'nome'
        role_username = 'mail'
        role_signup = True
        doc = True
        sequence = 4
        class_diagram = 'endereco', 'telefone', 'objeto', 'amigo'
        select_display = 'nome', 'mail', 'data_nascimento'
        pdf = True
        list_csv = True
        list_xls = True

    def __unicode__(self):
        return self.nome


class ObjetoManager(models.DefaultManager):

    @subset(u'Disponiveis para Empréstimo', can_view=u'Pessoa')
    def disponiveis(self):
        """
        Lista os objetos que não se encontram emprestados no momento.
        """
        return self.exclude(pk__in=Emprestimo.objects.filter(data_devolucao__isnull=True).values_list('objetos', flat=True))


class Objeto(models.Model):
    pessoa = models.ForeignKey(Pessoa, verbose_name=u'Pessoa')
    nome = models.CharField(u'Nome', search=True, example=u'Código de Da Vinci')
    categoria = models.ForeignKey(Categoria, verbose_name=u'Categoria', example=u'Livros')
    foto = models.ImageField(upload_to='objetos', null=True, blank=True, sizes=[(300, 300)])
    valor = models.DecimalField(verbose_name=u'Valor', null=True, example='17,00')

    fieldsets = (
        (u'Dados Gerais', {'fields': ('pessoa', 'nome', ('categoria', 'valor'), ('foto', 'is_emprestado')), 'extra': ('get_total_emprestimos_por_amigo',)}),
    )

    class Meta:
        verbose_name = u'Objeto'
        verbose_name_plural = u'Objetos'
        list_menu = u'Objetos'
        list_template = 'image_rows.html'
        list_display = 'foto', 'nome', 'valor'
        select_display = 'foto', 'nome', 'categoria', 'valor'
        icon = 'fa-archive'
        add_shortcut = 'Pessoa'
        list_shortcut = 'Pessoa',
        can_admin_by_role = 'Pessoa'
        doc = True
        sequence = 5
        class_diagram = 'categoria',

    def __unicode__(self):
        return self.nome

    @attr(u'Total de Empréstimo por Amigo', formatter='chart')
    def get_total_emprestimos_por_amigo(self):
        return self.emprestimo_set.count('amigo')

    @attr(u'Está emprestado?')
    def is_emprestado(self):
        return self.emprestimo_set.filter(data_devolucao__isnull=True).exists()


class Amigo(models.Model):
    pessoa = models.ForeignKey(Pessoa, verbose_name=u'Pessoa')
    nome = models.CharField(u'Nome', search=True, example=u'Hugo')
    data_nascimento = models.DateField(u'Data de Nascimento', example='02/07/1980')
    ativo = models.BooleanField(u'Ativo', default=True, filter=True)

    fieldsets = (
        (u'Dados Gerais', {'fields': ('pessoa', 'nome', ('data_nascimento', 'ativo')), }),
        (u'Estatística::Totais', {'extra': ('get_total_emprestimos',)},),
        (u'Histórico::Empréstimos', {'relations': ('get_emprestimos',)}, )
    )

    class Meta:
        verbose_name = u'Amigo'
        verbose_name_plural = u'Amigos'
        list_menu = u'Amigos'
        icon = 'fa-users'
        add_shortcut = 'Pessoa'
        list_shortcut = 'Pessoa'
        list_lookups = 'pessoa'
        can_admin_by_role = 'Pessoa'
        doc = True
        sequence = 6

    def __unicode__(self):
        return self.nome

    @attr(u'Total de Empréstimo por Objeto', formatter='statistics')
    def get_total_emprestimos(self):
        return self.emprestimo_set.count('amigo', 'objetos')

    @attr(u'Empréstimos')
    def get_emprestimos(self):
        return self.emprestimo_set.all()


class EmprestimoManager(models.DefaultManager):

    @subset(u'Todos', can_view=u'Pessoa', actions=('registrar_devolucao',))
    def all(self, *args, **kwargs):
        return super(EmprestimoManager, self).all(*args, **kwargs)

    @subset(u'Devolvidos', can_view=u'Pessoa', actions=('cancelar_devolucao', 'cancelar_devolucoes'))
    def devolvidos(self):
        """
        Lista os empréstimos que já foram devolvidos.
        """
        return self.filter(data_devolucao__isnull=False)

    @subset(u'Não-Devolvidos', can_view=u'Pessoa', actions=('registrar_devolucao', 'alterar_previsoes_devolucao'), alert=True, notify=True)
    def nao_devolvidos(self):
        """
        Lista os empréstimos que ainda não foram devolvidos.
        """
        return self.filter(data_devolucao__isnull=True)

    @action(u'Cancelar Devoluções', can_execute=u'Pessoa', condition='devolvidos')
    def cancelar_devolucoes(self):
        """
        Cancela a devolução dos registros selecionados.
        """
        self.update(data_devolucao=None)

    @action(u'Alterar Previsões de Devolução', can_execute=u'Pessoa', input='AlterarPrevisaoDevolucao', condition='nao_devolvidos')
    def alterar_previsoes_devolucao(self, data_prevista_devolucao):
        """
        Altera a previsão para devolução dos registros selecionados.
        """
        self.update(data_prevista_devolucao=data_prevista_devolucao)

    @attr(u'Total por Objeto', can_view='Pessoa', formatter='chart', dashboard='right')
    def get_total_por_objeto(self):
        return self.count('objetos')

    @attr(u'Total por Amigo', can_view='Administrador', dashboard='right')
    def get_total_por_amigo(self):
        return self.all().count('amigo')

    @attr(u'Total por Amigo', can_view='Administrador', formatter='bar_chart', dashboard='right')
    def get_total_por_amigo2(self):
        return self.all().count('amigo')

    @attr(u'Últimos Emprestimos', can_view='Pessoa', dashboard='center')
    def get_utimos_emprestimos(self):
        return self.all(self.user).order_by('-id')


class Emprestimo(models.Model):
    """
    Os empréstimos de objetos são realizados pelas pessoas para os seus amigos.
    """
    pessoa = models.ForeignKey(Pessoa, verbose_name=u'Pessoa', filter=True)
    objetos = models.ManyToManyField(Objeto, verbose_name=u'Objetos', form_filter='pessoa', filter=True, example=u'Código de Da Vinci', lazy=True)
    amigo = models.ForeignKey(Amigo, verbose_name=u'Amigo', search=('nome',), form_filter='pessoa', lazy=True, example=u'Hugo', display='detail')
    data_emprestimo = models.DateField(u'Data do Empréstimo', filter=True, example='01/01/2018')
    data_prevista_devolucao = models.DateField(u'Data Prevista Devolução', example='02/01/2018')
    data_devolucao = models.DateField(u'Data da Devolução', null=True, exclude=True, example='02/01/2018')

    fieldsets = (
        (u'Dados Gerais', {'fields': ('pessoa', ('amigo', 'data_emprestimo'), 'objetos'), 'extra': ('get_status',)}),
        (u'Período do Empréstimo', {'fields': (('data_prevista_devolucao', 'get_percentual_prazo_decorrido',),)}),
        (u'Dados da Devolução', {'fields': ('data_devolucao',), 'actions': ('registrar_devolucao', 'cancelar_devolucao')}),
    )

    class Meta:
        verbose_name = u'Empréstimo'
        verbose_name_plural = u'Empréstimos'
        list_menu = u'Empréstimos'
        list_per_page = 6
        icon = 'fa-calendar-o'
        add_shortcut = 'Pessoa'
        list_shortcut = True#'Pessoa'
        list_lookups = 'pessoa'
        can_admin_by_role = 'Pessoa'
        add_label = u'Efetuar Empréstimo'
        doc = True
        sequence = 7
        class_diagram = True

    def __unicode__(self):
        return u'Empréstimo #%s' % self.pk

    def initial(self):
        return dict(data_emprestimo=datetime.date.today())

    @attr(u'Prazo Decorrido (%)', formatter='progress')
    def get_percentual_prazo_decorrido(self):
        dias_para_entrega = (self.data_prevista_devolucao-self.data_emprestimo).days
        dias_docorridos = (datetime.date.today()-self.data_emprestimo).days
        return dias_para_entrega and dias_docorridos*100/dias_para_entrega or 100

    @action(u'Alterar Previsão de Devolução', can_execute='Pessoa', condition='not data_devolucao', sequence=7.1)
    def alterar_previsao_devolucao(self, data_prevista_devolucao):
        """
        Altera a "Data Prevista para Devoluação" com a data informada.
        """
        if data_prevista_devolucao < self.data_emprestimo:
            raise ValidationError(u'A data prevista para devolução deve ser maior ou igual a data do empréstimo.')
        self.data_prevista_devolucao = data_prevista_devolucao
        self.save()

    @action(u'Registrar Devolução', can_execute='Pessoa', condition='not data_devolucao', category=u'Ações', inline=True, message=u'Devolução registrada com sucesso', sequence=7.2)
    def registrar_devolucao(self, data_devolucao):
        """
        Registra a devolução, registrando o valor da "Data de Devoluação" com a data informada.
        """
        if data_devolucao < self.data_emprestimo:
            raise ValidationError(u'A data da devolução não pode ser inferior à data do empréstimo.')
        self.data_devolucao = data_devolucao
        self.save()

    def registrar_devolucao_initial(self):
        return dict(data_devolucao=datetime.date.today())

    @action(u'Cancelar Devolução', can_execute='Pessoa', condition='pode_cancelar_devolucao', inline=False, sequence=7.3)
    def cancelar_devolucao(self):
        """
        Cancela a devolução, registrando o valor da "Data de Devoluação" como nulo.
        """
        self.data_devolucao = None
        self.save()

    def pode_cancelar_devolucao(self):
        return self.data_devolucao is not None

    @attr(u'Status', formatter='timeline')
    def get_status(self):
        if not self.data_devolucao and self.data_prevista_devolucao <= datetime.date.today():
            return (u'Empréstimo', self.data_emprestimo), (u'Previsão da Devolução', self.data_prevista_devolucao), (u'Devolução', self.data_devolucao)
        else:
            return (u'Empréstimo', self.data_emprestimo), (u'Devolução', self.data_devolucao)

    def save(self):
        if not self.pk and self.data_emprestimo < datetime.date.today():
            raise ValidationError(u'A data do empréstimo deve ser maior do que a data atual.')
        super(Emprestimo, self).save()





