# -*- coding: utf-8 -*-
from djangoplus.admin.models import User
from djangoplus.test import TestCase
from django.conf import settings


class AppTestCase(TestCase):

    def cancelar_devolucao_em_emprestimo(self):
        self.click_menu(u'Empréstimos')
        self.click_icon(u'Visualizar')
        self.click_button(u'Cancelar Devolução')
        self.click_icon(u'Principal')

    def registrar_devolucao_em_emprestimo(self):
        self.click_menu(u'Empréstimos')
        self.click_icon(u'Visualizar')
        self.click_button(u'Registrar Devolução')
        self.look_at_popup_window()
        self.enter(u'Data da Devolução', u'02/01/2018')
        self.click_button(u'Registrar Devolução')
        self.click_icon(u'Principal')

    def alterar_previsao_devolucao_em_emprestimo(self):
        self.click_menu(u'Empréstimos')
        self.click_icon(u'Visualizar')
        self.click_button(u'Alterar Previsão de Devolução')
        self.look_at_popup_window()
        self.enter(u'Data Prevista Devolução', u'02/01/2018')
        self.click_button(u'Alterar Previsão de Devolução')
        self.click_icon(u'Principal')

    def efetuar_emprestimo(self):
        self.click_menu(u'Empréstimos')
        self.click_button(u'Efetuar Empréstimo')
        self.choose(u'Amigo', u'Hugo')
        self.enter(u'Data do Empréstimo', u'01/01/2018')
        self.enter(u'Data Prevista Devolução', u'02/01/2018')
        self.choose(u'Objetos', u'Código de Da Vinci')
        self.click_button(u'Cadastrar')
        self.click_icon(u'Principal')

    def cadastrar_amigo(self):
        self.click_menu(u'Amigos')
        self.click_button(u'Cadastrar')
        self.enter(u'Nome', u'Hugo')
        self.enter(u'Data de Nascimento', u'02/07/1980')
        self.click_button(u'Cadastrar')
        self.click_icon(u'Principal')

    def cadastrar_objeto(self):
        self.click_menu(u'Objetos')
        self.click_button(u'Cadastrar')
        self.enter(u'Nome', u'Código de Da Vinci')
        self.choose(u'Categoria', u'Livros')
        self.enter(u'Valor', u'17,00')
        self.click_button(u'Cadastrar')
        self.click_icon(u'Principal')

    def cadastrar_pessoa(self):
        self.click_menu(u'Usuários', u'Pessoas')
        self.click_button(u'Cadastrar')
        self.enter(u'Nome', u'Carlos Breno')
        self.enter(u'E-mail', u'brenokcc@yahoo.com.br')
        self.enter(u'Data de Nascimento', u'27/08/1984')
        self.click_button(u'Cadastrar')
        self.click_icon(u'Principal')

    def cadastrar_categoria(self):
        self.click_menu(u'Cadastros', u'Gerais', u'Categorias')
        self.click_button(u'Cadastrar')
        self.enter(u'Descrição', u'Livros')
        self.click_button(u'Cadastrar')
        self.click_icon(u'Principal')

    def cadastrar_administrador(self):
        self.click_menu(u'Usuários', u'Administradores')
        self.click_button(u'Cadastrar')
        self.enter(u'Nome', u'Admin')
        self.enter(u'E-mail', u'admin@emprestimos.com')
        self.click_button(u'Cadastrar')
        self.click_icon(u'Principal')

    def test_app(self):

        User.objects.create_superuser('admin', None, settings.DEFAULT_PASSWORD)

        self.login(u'admin', settings.DEFAULT_PASSWORD)

        # Acessar como Superusuário
        if self.login(u'admin', u'senha'):
            # Cadastrar Administrador
            self.cadastrar_administrador()
        # Acessar como Administrador
        if self.login(u'admin@emprestimos.com', u'senha'):
            # Cadastrar Categoria
            self.cadastrar_categoria()
            # Cadastrar Pessoa
            self.cadastrar_pessoa()
        # Acessar como Pessoa
        if self.login(u'brenokcc@yahoo.com.br', u'senha'):
            # Cadastrar Objeto
            self.cadastrar_objeto()
            # Cadastrar Amigo
            self.cadastrar_amigo()
            # Efetuar Empréstimo
            self.efetuar_emprestimo()
            # Alterar Previsão de Devolução em Empréstimo
            self.alterar_previsao_devolucao_em_emprestimo()
            # Registrar Devolução em Empréstimo
            self.registrar_devolucao_em_emprestimo()
            # Cancelar Devolução em Empréstimo
            self.cancelar_devolucao_em_emprestimo()
        # Acessar como Administrador
        if self.login(u'admin@emprestimos.com', u'senha'):
            # Relatório de Totais
            pass# self.xxxx()